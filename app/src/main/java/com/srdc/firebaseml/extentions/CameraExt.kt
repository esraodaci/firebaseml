package com.srdc.firebaseml.extentions

import android.graphics.Bitmap
import android.graphics.Matrix
import android.media.ExifInterface
import android.view.View

/**
 * Created by
 * esraodaci on 2019-12-06...
 */

fun View.visible(){
    visibility = View.VISIBLE
}

fun View.gone(){
    visibility = View.GONE
}

fun Bitmap.rotate(myBitmap: Bitmap, exifInterface:ExifInterface){
     var rotatedBitmap: Bitmap? = null

    val orientation: Int = exifInterface.getAttributeInt(
        ExifInterface.TAG_ORIENTATION,
        ExifInterface.ORIENTATION_UNDEFINED
    )

    when (orientation) {
        ExifInterface.ORIENTATION_ROTATE_90 ->
            rotatedBitmap = rotateImage(myBitmap, 90f)
        ExifInterface.ORIENTATION_ROTATE_180 ->
            rotatedBitmap = rotateImage(myBitmap, 180f)
        ExifInterface.ORIENTATION_ROTATE_270 ->
            rotatedBitmap = rotateImage(myBitmap, 270f)
        ExifInterface.ORIENTATION_NORMAL -> myBitmap
        else -> rotatedBitmap = myBitmap
    }
}

fun rotateImage(source: Bitmap, angle: Float): Bitmap? {
    val matrix = Matrix()
    matrix.postRotate(angle)
    return Bitmap.createBitmap(
        source, 0, 0, source.width, source.height,
        matrix, true
    )
}